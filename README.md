# Project Report: 3D Ball Collision in Godot

## Functionality Implemented:

- **3D Ball Movement**: Balls in a 3D space can move freely, and their movement is influenced by their direction and speed.
- **Wall Collisions**: Balls reflect off walls with perfect elasticity. This ensures that balls don't pass through the walls and instead bounce back into the play area.
- **Ball Collisions**: Balls also reflect off each other when they collide, ensuring realistic interactions between them.

## Interesting Features:

- **Optimized Collision Detection**: Instead of checking for collisions with every other ball, the program uses a VectorManager to efficiently manage and retrieve ball vectors. This reduces the computational overhead and makes the game run smoother, especially when there are many balls in the scene.
- **Dynamic Ball Initialization**: Depending on the number of balls already present, new balls are initialized at different positions to avoid immediate collisions.

## Software Dependencies:

- **Godot Engine**: The game relies on the Godot game engine for rendering and physics.
- **Godot C++ API**: The game's logic is implemented using the Godot C++ API, allowing for more optimized and complex operations than the standard GDScript.

## Issues Encountered:

- **Collision Response**: Initially, balls were not reflecting correctly upon collision. This was due to an error in the collision response calculation. The issue was resolved by adjusting the reflection logic.
- **Unique Ball Identification**: Ensuring each ball had a unique identifier was crucial for efficient collision detection. This was achieved using the VectorManager class.

## Demo Video:

![3D Ball Collision Demo](./demo.mp4)

## Installation:

### Prerequisites:

Ensure you have the Godot engine and the necessary C++ bindings. If you're unfamiliar with the process, you can follow the GDExtension tutorial for a detailed guide.

### Building the C++ bindings:

1. **Generate Extension API File (if needed)**: If you're using a newer version of Godot, generate the extension API file:
    ```bash
    godot --dump-extension-api extension_api.json
    ```
    Place the `extension_api.json` file in the project folder.

2. **Compile the Bindings**: Navigate to the `godot-cpp` directory and compile the bindings. Replace `<platform>` with your OS (windows, linux, or macos). To speed up the compilation, add `-jN` at the end, where N is the number of CPU threads on your system (e.g., 4 threads).
    
        cd godot-cpp
        scons platform=<platform> -j4 custom_api_file=<PATH_TO_FILE>
        cd ..


### Clone the Repository:

    
    git clone <REPO_URL>
    
### Set Up a Virtual Environment:

    
    python -m venv venv
    source venv/bin/activate  # On Windows, use `venv\Scripts\activate`
    
### Install Dependencies:

    
    pip install -r requirements.txt
    

### Code Layout & Implementation:
- **VectorManager Class**: Manages the 3D vectors of all balls. It can add, delete, and retrieve balls based on unique IDs.
- addVector(): Adds a ball vector and returns its unique ID.
- deleteVector(): Removes a ball vector based on its ID.
- getVector(): Retrieves a ball vector using its ID.

- **Ball3D Class**: Represents a ball in the 3D space.
- _process(): Handles the movement of the ball, wall collisions, and ball-to-ball collisions.
- collides_with(): Checks if the ball collides with another ball and handles the collision response.
- set_speed() & get_speed(): Set and retrieve the speed of the ball, respectively.


