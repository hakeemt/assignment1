#include "ball3D.h"
#include <godot_cpp/core/class_db.hpp>
#include <ctime>
#include <cstdlib>
#include <godot_cpp/godot.hpp>
#include <unordered_map>
#include <memory>

namespace godot
{
    // VectorManager class manages the 3D vectors of all balls.
    class VectorManager
    {
    private:
        int nextID = 0; // Counter to generate unique IDs for balls.

    public:
        // Map to store Ball3D pointers with their unique IDs.
        std::unordered_map<int, Ball3D *> ballMap;

        // Adds a ball to the map and returns its unique ID.
        int addVector(Ball3D *ball)
        {
            int id = nextID++;
            ballMap[id] = ball;
            return id;
        }

        // Removes a ball from the map using its ID.
        void deleteVector(int id)
        {
            ballMap.erase(id);
        }

        // Retrieves a ball from the map using its ID.
        Ball3D *getVector(int id)
        {
            return ballMap[id];
        }
    };

    // Global instance of VectorManager.
    VectorManager vectorMgr;

    // Constructor for Ball3D class.
    Ball3D::Ball3D()
    {
        int uniqueID;

        time_passed = 0.0;
        time_emit = 0.0;
        speed = 1.0; // Default speed in units per second.
        position = Vector3(2, 2, 2);
        // Randomly generate a normalized direction for the ball.
        direction = Vector3(
                        (rand() % 2001 - 1000) / 1000.0f,
                        (rand() % 2001 - 1000) / 1000.0f,
                        (rand() % 2001 - 1000) / 1000.0f)
                        .normalized();
    }

    // Destructor for Ball3D class.
    Ball3D::~Ball3D()
    {
        vectorMgr.deleteVector(uniqueID);
    }

    // Initializes the ball's position based on the number of balls present.
    void Ball3D::initialize()
    {
        uniqueID = vectorMgr.addVector(this);
        double D = 4.0;

        int numBalls = vectorMgr.ballMap.size();
        // Set position based on the number of balls.
        if (numBalls == 1)
        {
            position = Vector3(2, 2, 2);
        }
        else if (numBalls == 2)
        {
            position = Vector3(2 + D, 2, 2);
        }
        else if (numBalls == 3)
        {
            position = Vector3(2 + D, 2 + D, 2);
        }

        set_position(position);
    }

    void Ball3D::_process(double delta)
    {
        time_passed += delta;
        Vector3 new_position = position + direction * (speed * delta); // Update based on direction

        // Check for wall collisions
        for (int i = 0; i < 3; i++)
        {
            if (new_position[i] <= 0.15) // Adjusted for ball's radius
            {
                new_position[i] = 0.15; // Ensure ball doesn't go past the wall
                Vector3 normal;
                normal[i] = 1;                                              // Normal for left/bottom/front walls
                direction = direction - 2 * normal * direction.dot(normal); // Reflect direction
            }
            else if (new_position[i] >= 2.15) // Adjusted for ball's radius
            {
                new_position[i] = 2.15; // Ensure ball doesn't go past the wall
                Vector3 normal;
                normal[i] = -1;                                             // Normal for right/top/back walls
                direction = direction - 2 * normal * direction.dot(normal); // Reflect direction
            }
        }

        // Check for ball collisions
        for (const auto &pair : vectorMgr.ballMap)
        {
            if (pair.first != this->uniqueID) // Don't check collision with itself
            {
                Ball3D *otherBall = pair.second;
                if (collides_with(*otherBall))
                {
                    // Compute the collision response for both balls
                    Vector3 normal = (position - otherBall->position).normalized();
                    direction = direction - 2 * normal * direction.dot(normal);
                    otherBall->direction = otherBall->direction - 2 * normal * otherBall->direction.dot(normal);
                }
            }
        }

        // Update ball's position
        position = new_position;
        set_position(position);
    }

    bool Ball3D::collides_with(Ball3D &otherBall)
    {
        Vector3 diff = position - otherBall.position;
        double distance = diff.length();
        double combinedRadii = 1 + 1; // Assuming each ball has a radius of 1

        if (distance < combinedRadii)
        {
            return true;
        }
        return false;
    }

    void Ball3D::set_speed(const double p_speed)
    {
        speed = p_speed;
    }

    double Ball3D::get_speed() const
    {
        return speed;
    }

    void Ball3D::_bind_methods()
    {
        // Binding our methods for Godot to recognize them
        ClassDB::bind_method(D_METHOD("initialize"), &Ball3D::initialize);
    }

} // namespace godot