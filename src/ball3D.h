#ifndef BALL3D_H
#define BALL3D_H

#include <godot_cpp/classes/sprite3d.hpp>

namespace godot
{

    class Ball3D : public Sprite3D
    {
        GDCLASS(Ball3D, Sprite3D)

    private:
    public:
        void set_speed(const double p_speed);
        double get_speed() const;
        Vector3 position;
        Vector3 direction;
        double time_passed;
        double speed;
        double uniqueID;

        double time_emit;
        bool collides_with(Ball3D &otherBall);

    protected:
        static void _bind_methods();

    public:
        Ball3D();
        ~Ball3D();

        void _process(double delta);
        void initialize();

    private:
    };
}

#endif